# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180711122243) do

  create_table "farms", force: :cascade do |t|
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "crop_name",     limit: 255
    t.string   "farm_location", limit: 255
    t.string   "month",         limit: 255
    t.string   "duration",      limit: 255
    t.string   "farm_size",     limit: 255
    t.string   "crop_logo",     limit: 255
    t.text     "description",   limit: 65535
    t.decimal  "amount",                      precision: 8, scale: 2
    t.string   "returns",       limit: 255
    t.boolean  "status"
  end

  create_table "payment_states", force: :cascade do |t|
    t.string   "transaction_id",       limit: 255
    t.string   "response_code",        limit: 255
    t.decimal  "amount_after_charges",             precision: 10
    t.string   "client_reference",     limit: 255
    t.string   "description",          limit: 255
    t.string   "external_trans_id",    limit: 255
    t.decimal  "amount",                           precision: 10
    t.decimal  "charges",                          precision: 10
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.string   "customer_name",          limit: 255
    t.string   "customer_mobile_number", limit: 255
    t.string   "channel",                limit: 255
    t.decimal  "amount",                             precision: 10
    t.string   "description",            limit: 255
    t.string   "token",                  limit: 255
    t.boolean  "status"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "farm_id",                limit: 4
    t.integer  "user_id",                limit: 4
    t.string   "u_code",                 limit: 255
    t.string   "trans_description",      limit: 255
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "fullname",               limit: 255
    t.string   "mobile_number",          limit: 255
    t.string   "username",               limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
