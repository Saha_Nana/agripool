class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :customer_name
      t.string :customer_mobile_number
      t.string :channel
      t.decimal :amount
      t.string :description
      t.string :token
      t.boolean :status

      t.timestamps null: false
    end
  end
end
