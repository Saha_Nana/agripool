class CreatePaymentStates < ActiveRecord::Migration
  def change
    create_table :payment_states do |t|
      t.string :transaction_id
      t.string :response_code
      t.decimal :amount_after_charges
      t.string :client_reference
      t.string :description
      t.string :external_trans_id
      t.decimal :amount
      t.decimal :charges

      t.timestamps null: false
    end
  end
end
