json.extract! transaction, :id, :customer_name, :customer_mobile_number, :channel, :amount, :description, :token, :status, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
