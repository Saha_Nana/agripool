json.extract! payment_state, :id, :transaction_id, :response_code, :amount_after_charges, :client_reference, :description, :external_trans_id, :amount, :charges, :created_at, :updated_at
json.url payment_state_url(payment_state, format: :json)
