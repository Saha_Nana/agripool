class TransactionsController < ApplicationController
require 'base64'
#require 'sinatra'
require 'json'
require 'nokogiri'
#require 'sinatra/activerecord'
require 'yaml'
require 'faraday'
require 'net/http'
require 'uri'
require 'savon'
require 'digest/md5'
require 'openssl'
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  
CLIENT_ID = 'dtajuwmo'
CLIENT_SECRET ='xgvbtkvc' 
CALLBACK_URL = "http://144.217.165.180:6007/primary_callback"  
DESCRIPTION = "Agripool contribution"
REQHDR = {'Content-Type'=>'Application/json','timeout'=>'180'}

def uniqueCode
  time=Time.new
  strtm = time.strftime("AGR"+"%y%m%d%L%H%M")
  return strtm
end

def tranSaver(customer_name,customer_mobile_number,amount,u_code,channel,token,farm_id,user_id,trans_description,status)
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')

  Transaction.create(customer_name: customer_name, customer_mobile_number: customer_mobile_number,amount: amount,
                            u_code: u_code, channel: channel,token: token, farm_id: farm_id, user_id: user_id ,trans_description: trans_description,status: status,
                            created_at: time)
end


def mobileMoneyReq(customerName,customerMsisdn,channel,amount,primaryCallbackUrl,description,token,farm_id,user_id,status)

  clientReference=uniqueCode
  url = 'https://api.hubtel.com'
  url_endpoint = '/v1/merchantaccount/merchants/HM0307180001/receive/mobilemoney'
 
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
   end
   
   
   tranSaver(customerName,customerMsisdn,amount,clientReference,channel,token,farm_id,user_id,description,status)
   
   # @transaction = Transaction.new({:customer_name => customer_name,:customer_mobile_number => customer_mobile_number, :amount => amount ,:channel=> channel,:token=> token,:status=> status,:farm_id=> farm_id,:user_id=> user_id})
   # @transaction.save
   
payload={
     :CustomerName => customerName,
     :CustomerMsisdn => customerMsisdn,
     :Channel => channel,
     :Amount => amount,
     :PrimaryCallbackUrl => primaryCallbackUrl,
     :Description => description,
     :ClientReference=> clientReference,
     :Token => token
 }

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

auth = 'Basic ' + Base64.encode64( "#{CLIENT_ID}:#{CLIENT_SECRET}" ).chomp

   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{auth}"
          req.body = json_payload
          logger.info req.url url_endpoint
   end
        logger.info "------------------------------------------"
        logger.info "Result from API: #{res.body}"
        logger.info "----------------------------------------------"
end


# def processPayment(customerName,customerMsisdn,channel,amount,token) 
# 
   # mobileMoneyReq(customerName,customerMsisdn,channel,amount,CALLBACK_URL,DESCRIPTION,token)
# 
# end

def contribute
   @farm = Farm.new
     @make_transaction = Transaction.new
   logger.info "---------------------------Contribute PARAMS---------------------------------"
    customer_name = params[:transaction][:customer_name]
    customer_mobile_number = params[:transaction][:customer_mobile_number]
    amount = params[:transaction][:amount]
    channel = params[:transaction][:channel]
    token = params[:transaction][:token]
    status = params[:transaction][:status] 
    farm_id = params[:transaction][:farm_id] 
    user_id = params[:transaction][:user_id] 
    
      
    mobileMoneyReq(customer_name,customer_mobile_number,channel,amount,CALLBACK_URL,DESCRIPTION,token,farm_id,user_id,status)
   
 render body: "In progress"
    
end




  # GET /transactions
  # GET /transactions.json
  def index
    @transactions = Transaction.all
     @farm = Farm.new
     @make_transaction = Transaction.new
     
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to @transaction, notice: 'Transaction was successfully created.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to @transaction, notice: 'Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:customer_name, :customer_mobile_number, :channel, :amount, :description, :token, :status)
    end
end
