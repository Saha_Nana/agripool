class FarmsController < ApplicationController
  before_action :set_farm, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user! ,only: [:pay]
  # before_filter :authenticate_user!
  # GET /farms
  # GET /farms.json
  def index
    @user = User.new
    @farms = Farm.all
    @make_transaction = Transaction.new
  end

  # GET /farms/1
  # GET /farms/1.json
  def show
  end

  def details
    
    if current_user

    @farms = Farm.all.order('id desc').limit(2)
    @make_transaction = Transaction.new
    @farm_id = params[:id]
    logger.info "Farms id is #{@farm_id}"
    logger.info "--------------------------------------"
    @farm_details = Farm.where(id: @farm_id)
    @customer_name = User.find_by(id: current_user.id)

    logger.info "Farms details is #{@farm_details.inspect}"
    logger.info "@customer_name in details is #{@customer_name.fullname}"
    logger.info "--------------------------------------"
    
    else
     @farms = Farm.all.order('id desc').limit(2)
    @make_transaction = Transaction.new
    @farm_id = params[:id]
    logger.info "Farms id is #{@farm_id}"
    logger.info "--------------------------------------"
    @farm_details = Farm.where(id: @farm_id)
    logger.info "Farms details is #{@farm_details.inspect}"
    logger.info "--------------------------------------"
    end
  end

  # def pay
  #
  # @make_transaction = Transaction.new
  #
  # @farm_id = params[:id]
  # logger.info "--------------------------------------"
  # logger.info "Farms id IN pay method is #{@farm_id}"
  # logger.info "--------------------------------------"
  #
  # respond_to do |format|
  # format.html
  # format.js
  # end
  #
  # end

  # GET /farms/new
  def new
    @farm = Farm.new
  end

  # GET /farms/1/edit
  def edit
  end

  # POST /farms
  # POST /farms.json
  def create
    @farm = Farm.new(farm_params)

    respond_to do |format|
      if @farm.save
        format.html { redirect_to @farm, notice: 'Farm was successfully created.' }
        format.json { render :show, status: :created, location: @farm }
      else
        format.html { render :new }
        format.json { render json: @farm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farms/1
  # PATCH/PUT /farms/1.json
  def update
    respond_to do |format|
      if @farm.update(farm_params)
        format.html { redirect_to @farm, notice: 'Farm was successfully updated.' }
        format.json { render :show, status: :ok, location: @farm }
      else
        format.html { render :edit }
        format.json { render json: @farm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farms/1
  # DELETE /farms/1.json
  def destroy
    @farm.destroy
    respond_to do |format|
      format.html { redirect_to farms_url, notice: 'Farm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_farm
    @farm = Farm.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def farm_params
    params.require(:farm).permit(:status,:crop_name, :farm_location, :month,:duration,:farm_size, :crop_logo, :description,:amount, :returns)
  end
end
